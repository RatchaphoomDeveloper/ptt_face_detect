from django.apps import AppConfig


class FacePttApiConfig(AppConfig):
    name = 'face_ptt_api'
